import os
from pathlib import Path
import pandas as pd
pd.set_option("display.max_colwidth", None)

# +
# convert "," sep to ";" 
dir2nomenclature = Path("../nomenclatures/")
datasets = ["OPENBIO", "OPENMEDIC", "OPENLPP"]

dataset = datasets[0]
for dataset in datasets:
    for f in os.listdir(Path(dir2nomenclature, dataset)):
        filepath =  Path(dir2nomenclature, dataset, f)
        if filepath.suffix in [".csv", ".CSV"]:
            df = pd.read_csv(filepath, sep=",")
            df.to_csv(filepath, sep=";", index=False)
# -

#primary key for openbio and openlpp
# openbio
path2nom = Path(dir2nomenclature, "OPENBIO", "openbio_ACTE.csv")
openbio_acte = pd.read_csv(path2nom, sep=";")
openbio_acte = openbio_acte.drop_duplicates(subset="ACTE")
openbio_acte.to_csv(path2nom, sep=";", index=False)

# openlpp
path2nom = Path(dir2nomenclature, "OPENLPP", "openlpp_TITRE.csv")
openlpp_acte = pd.read_csv(path2nom, sep=";")
openlpp_acte = openlpp_acte.drop_duplicates(subset="TITRE")
#openlpp_acte.to_csv(path2nom, sep=";", index=False)


